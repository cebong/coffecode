<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Home extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
		}
	
		public function index()
		{
			$data = array(
				'produk' => $this->m_produk->list_produk()->result(),
				'testimonial' => $this->m_testimoni->list_testimoni()->result(),
				'portfolio' => $this->m_portfolio->list_portfolio()->result(),
				'klien' => $this->m_klien->list_klien()->result(),
			);
			$this->load->view('home/header');
			$this->load->view('home/index',$data);
			$this->load->view('home/footer');
		}

		function login(){
			$this->load->view('login');
		}

		function register(){
			$this->load->view('register');
		}

		function contact(){
			$email = $this->input->post('email');
			$subject = $this->input->post('subject');
			$message = $this->input->post('message');

			require_once(APPPATH.'libraries/PHPMailer/PHPMailerAutoload.php');
            $mail = new PHPMailer;
         
            $mail->isSMTP();
            $mail->Host = 'mail.ajarincode.com'; //nama "domain" ganti sesuai nama domain anda. misal domain anda satuan.com maka bentuk host mailnya adalah mail.satuan.com
            $mail->SMTPAuth = false;
            $mail->SMPTSecure = false;
            $mail->Username = 'support@coffecode.tesch'; //email dari domain anda, untuk cara pembuatan email akan di bahas di bawah
            $mail->Password = 'MasanemasusasE'; //masukan kata sandi
            $mail->Port = 587; //port tidak usah di ubah, biarkan 587
                     
            $mail->setFrom($email); //email pengirim
            $mail->addAddress('support@coffecode.tech', 'penerima'); //email penerima
            $mail->isHTML(true);
                     
            ///atur pesan email disini
            $mail->Subject = $subject;
            $mail->Body    = $message;
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
                         
            if(!$mail->send()) {
                echo 'Message could not be sent.';
                echo 'Mailer Error: ' . $mail->ErrorInfo;
            }
		}
	
	}
	
	/* End of file Home.php */
	/* Location: ./application/controllers/Home.php */
?>