<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Auth extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
		}
	
		function login(){
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$cek = $this->m_auth->cek_login($username,'user');

			if($cek->num_rows() > 0){
				$data = $cek->row();
				if(password_verify($password,$data->password)){
					if($data->confirm == "T"){
						$notif = array(
							'status' => "gagal",
							'message' => "Silahkan lakukan konfirmasi via email terlebih dahulu",
						);
						$this->session->set_flashdata($notif);
						redirect('login');
					}else{
						$session = array(
							'id' => $data->id_user,
							'nama_user' => $data->nama_user,
							'jk' => $data->jk,
							'phone' => $data->phone,
							'alamat' => $data->alamat,
							'username' => $data->username,
							'email' => $data->email,
							'path' => $data->path,
							'confirm' => $data->confirm,
							'hak_akses' => $data->hak_akses,
						);
						$this->session->set_userdata($session);
						redirect('dashboard');
					}
				}else{
					$notif = array(
						'status' => "gagal",
						'message' => "Maaf Username dan Password Tidak Cocok",
					);
					$this->session->set_flashdata($notif);
					redirect('login');
				}
			}else{
				$notif = array(
					'status' => "gagal",
					'message' => "Maaf Username Tidak Terdaftar",
				);
				$this->session->set_flashdata($notif);
				redirect('login');
			}
		}

		function logout(){
			$this->session->sess_destroy();
			redirect('login');
		}

		function register(){
			$this->form_validation->set_rules('nama','Nama Lengkap','required|trim');
			$this->form_validation->set_rules('phone','No Handhpone','required|numeric');
			$this->form_validation->set_rules('username','Username','required|trim');
			$this->form_validation->set_rules('email','Email','required|trim');
			$this->form_validation->set_rules('password','Password','required|trim');

			if($this->form_validation->run() == FALSE){
				$notif = array(
					'status' => "gagal",
					'message' => validation_errors(),
				);
				$this->session->set_flashdata($notif);
				redirect('register');
			}else{
				if($this->input->post('cek') != true){
					$notif = array(
						'status' => "gagal",
						'message' => "Silahkan centang persetujuan terlebih dahulu",
					);
					$this->session->set_flashdata($notif);
					redirect('register');
				}else{
					$data = array(
						'nama_user' => $this->input->post('nama'),
						'jk' => $this->input->post('jk'),
						'phone' => $this->input->post('phone'),
						'username' => $this->input->post('username'),
						'email' => $this->input->post('email'),
						'password' => password_hash($this->input->post('password'),PASSWORD_BCRYPT),
						'path' => "default.png",
						'confirm' => "T",
						'hak_akses' => "Customer",
					);
					$this->m_auth->daftar($data,'user');
					$notif = array(
						'status' => "berhasil",
						'message' => "Pendaftaran Berhasil, Silahkan Konfirmasi Akun Anda Melalui E-Mail",
					);
					$this->session->set_flashdata($notif);
					redirect('login');
				}
			}
		}
	
	}
	
	/* End of file Auth.php */
	/* Location: ./application/controllers/Auth.php */
?>