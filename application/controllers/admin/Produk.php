<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Produk extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
		}
	
		public function index()
		{
			$data = array('produk' => $this->m_produk->list_produk()->result());
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/master/produk/index',$data);
			$this->load->view('dashboard/footer');
		}

		function create(){
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/master/produk/create');
			$this->load->view('dashboard/footer');
		}

		function tambah(){
			$gambar = $_FILES['gambar']['name'];
			$config['upload_path'] = './assets/images/produk/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['file_name'] = $this->input->post('produk');
			$config['remove_spaces'] = TRUE;
			$config['overwrite'] = TRUE;
			$config['file_ext_tolower'] = TRUE;
			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('gambar')){
				$notif = array(
					'status' => "gagal",
					'message' => $this->upload->display_errors(),
				);
				$this->session->set_flashdata($notif);
				redirect('admin/produk/create');
			}else{
				$data = array(
					'id_user' => $this->input->post('id'),
					'nama_produk' => $this->input->post('produk'),
					'harga_produk' => $this->input->post('harga'),
					'detail_produk' => $this->input->post('deskripsi'),
					'path_produk' => $this->upload->data('file_name'),
					'status_produk' => "Open",
				);
				$notif = array(
					'status' => "berhasil",
					'message' => "Produk Berhasil Ditambahkan",
				);
				$this->session->set_flashdata($notif);
				$this->m_produk->create($data,'produk');
				redirect('admin/produk');
			}
		}
	
	}
	
	/* End of file Produk.php */
	/* Location: ./application/controllers/Produk.php */
?>