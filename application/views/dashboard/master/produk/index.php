<div class="content-wrapper">
	<section class="content-header">
		<h1>Daftar Produk</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<a href="<?php echo site_url('admin/produk/create') ?>" class="btn btn-success btn-flat"><i class="fa fa-plus"></i> Tambah</a>
						<table class="table table-bordered table-striped" id="table">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Produk</th>
									<th class="text-center">Harga</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach($produk as $p){ ?>
								<tr>
									<td class="text-center"><?php echo $no++ ?></td>
									<td><?php echo $p->nama_produk ?></td>
									<td>Rp <?php echo number_format($p->harga_produk,2,',','.') ?></td>
									<td class="text-center">
										<a href="" class="btn btn-info btn-xs btn-flat"><i class="fa fa-edit"></i></a>
										<a href="" class="btn btn-danger btn-xs btn-flat"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>