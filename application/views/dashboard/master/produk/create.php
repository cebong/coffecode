<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<?php echo form_open_multipart('admin/produk/tambah') ?>
							<div class="form-group">
								<label class="control-label">Nama Produk :</label>
								<input type="text" name="produk" class="form-control">
							</div>
							<div class="form-group">
								<label class="control-label">Harga Produk :</label>
								<input type="text" name="harga" class="form-control">
							</div>
							<div class="form-group">
								<label class="control-label">Deskripsi :</label>
								<textarea name="deskripsi" id="summernote"></textarea>
							</div>
							<div class="form-group" id="my-awesome-dropzone">
								<label class="control-label">Gambar Produk :</label>
								<input type="file" name="gambar" class="dropify">
							</div>
							<div class="form-group">
								<input type="hidden" name="id" class="form-control" value="<?php echo $this->session->userdata('id') ?>">
								<button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>