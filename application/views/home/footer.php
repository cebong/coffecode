<!-- Start Footer -->
		<footer id="footer" class="wow fadeIn">
			<!-- Footer Top -->
			<div class="footer-top">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 ">
							<!-- Logo -->
							<div class="logo">
								<a href="<?php echo site_url('index') ?>">CoffeCode</a>
							</div>	
							<!--/ End Logo -->
							<!-- Social -->
							<ul class="social">
								<li class="active"><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-instagram"></i></a></li>
							</ul>	
							<!--/ End Social -->
						</div>
					</div>
				</div>
			</div>
			<!--/ End Footer Top -->
			
			<!-- Copyright -->
			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="text">
								<p>&copy; Copyright 2018<span><i class="fa fa-heart"></i></span>CoffeCoed</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--/ End Copyright -->
		</footer>
		<!--/ End Footer -->

		<!-- Jquery JS -->
		<script type="text/javascript" src="<?php echo base_url('assets/home/') ?>js/jquery.min.js"></script>
		
		<!-- Colors JS -->
		<script type="text/javascript" src="<?php echo base_url('assets/home/') ?>js/colors.js"></script>
		
		<!-- Google Map JS -->
		<script  type="text/javascript" src="<?php echo base_url('assets/home/') ?>js/gmap.js"></script>
		
		<!-- Modernizr JS -->
		<script type="text/javascript"  src="<?php echo base_url('assets/home/') ?>js/modernizr.min.js"></script>
	
		<!-- Appear JS-->
		<script  type="text/javascript" src="<?php echo base_url('assets/home/') ?>js/jquery.appear.js"></script>

		<!-- Animate JS -->
    	<script  type="text/javascript" src="<?php echo base_url('assets/home/') ?>js/wow.min.js"></script>
		
		<!-- Onepage Nav JS -->
    	<script  type="text/javascript" src="<?php echo base_url('assets/home/') ?>js/jquery.nav.js"></script>
		
		<!-- Yt Player -->
		<script  type="text/javascript" src="<?php echo base_url('assets/home/') ?>js/ytplayer.min.js"></script>
	
		<!-- Popup JS -->
		<script type="text/javascript"  src="<?php echo base_url('assets/home/') ?>js/jquery.magnific-popup.min.js"></script>

		<!-- Typed JS -->
		<script  type="text/javascript" src="<?php echo base_url('assets/home/') ?>js/typed.min.js"></script>
		
		<!-- Scroll Up JS -->
		<script  type="text/javascript" src="<?php echo base_url('assets/home/') ?>js/jquery.scrollUp.min.js"></script>
		
		<!-- Slick Nav JS -->
		<script  type="text/javascript" src="<?php echo base_url('assets/home/') ?>js/jquery.slicknav.min.js"></script>
		
		<!-- Counterup JS -->
		<script  type="text/javascript" src="<?php echo base_url('assets/home/') ?>js/waypoints.min.js"></script>
		<script  type="text/javascript" src="<?php echo base_url('assets/home/') ?>js/jquery.counterup.min.js"></script>
		
		<!-- Owl Carousel JS -->
		<script  type="text/javascript" src="<?php echo base_url('assets/home/') ?>js/owl.carousel.min.js"></script>
		
		<!-- Bootstrap JS -->
		<script  type="text/javascript" src="<?php echo base_url('assets/home/') ?>js/bootstrap.min.js"></script>
		
		<!-- Main JS -->
		<script type="text/javascript"  src="<?php echo base_url('assets/home/') ?>js/main.js"></script>
    </body>
</html>