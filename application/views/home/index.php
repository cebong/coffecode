<!-- Start Animate Text -->
		<section id="j-slider" >
			<div class="animate-text">
				<!-- Single Slider -->
				<div class="single-slider" style="background-image:url(<?php echo base_url('assets/home/') ?>images/background1.jpg);" >
					<div class="container">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="slide-text center">
									<h1><span class="info"></span></h1>
									<p>Jasa Pembuatan website dan Design Grafis</p>
									<div class="slide-button">
										<a href="#service" class="button primary">Service</a>
										<a href="#contact" class="button primary">Contact Us</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--/ End Single Slider -->
			</div>
		</section>
		<!--/ End Animate Text -->
		
		<!-- Start Service -->
		<section id="service" class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 wow fadeIn">
						<div class="section-title center">
							<h2>Our <span>Service</span></h2>
							<p>Berikut layanan yang kami tawarkan</p>
						</div>
					</div>
				</div>
				<div class="row">
					<!-- Single Service -->
					<div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.4s">
						<div class="single-service">
							<i class="fa fa-code"></i>
							<h2>Web Development</h2>
							<p>Pembuatan Aplikasi Web dan Sistem Informasi</p>
						</div>
					</div>
					<!--/ End Single Service -->
					<!-- Single Service -->
					<div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.6s">
						<div class="single-service">
							<i class="fa fa-500px"></i>
							<h2>Design Grafis</h2>
							<p>Pembuatan Design (Banner, Kartu Nama, Spanduk, Pamflet, dsb)</p>
						</div>
					</div>
					<!--/ End Single Service -->
					<!-- Single Service -->
					<div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.8s">
						<div class="single-service">
							<i class="fa fa-shopping-cart"></i>
							<h2>Jual Aplikasi</h2>
							<p>Penjualan Software Aplikasi</p>
						</div>
					</div>
					<!--/ End Single Service -->
				</div>
			</div>
		</section>
		<!--/ End Service -->
		
		<!-- Start About Us -->
		<section id="about-us" class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 wow fadeIn">
						<div class="section-title center">
							<h2>About <span>US</span></h2>
							<p>Coffe Code merupakan jasa pembuatan website dan design grafis yang siap membantu untuk keperluan bisnis anda, kami juga menjual dan membagikan beberapa produk web kami secara online</p>
						</div>
					</div>
				</div>
				<div class="row"> 
					<!-- About Image -->
					<div class="col-md-5 col-sm-12 col-xs-12 wow slideInLeft">
						<div class="about-main">
							<div class="about-img">
								<img src="<?php echo base_url('assets/images/') ?>logo.png" alt=""/>
							</div>
						</div>
					</div>
					<!--/ End About Image -->
					<div class="col-md-7 col-sm-12 col-xs-12 wow fadeIn" data-wow-delay="1s">
						<!-- About Tab -->
						<div class="tabs-main">
							<!-- Tab Nav -->
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active"><a href="#welcome"  data-toggle="tab">Welcome</a></li>
								<!-- <li role="presentation"><a href="#pesanweb" data-toggle="tab">Cara Pesan Via Web</a></li> -->
								<li role="presentation"><a href="#pesanchat" data-toggle="tab">Cara Pesan Via Chat</a></li>
							</ul>
							<!--/ End Tab Nav -->
							<!-- Tab Content -->
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane fade in active" id="welcome">
									<div class="row">
										<div class="col-md-6 col-sm-6 col-xs-12">
											<!-- Single Tab -->
											<div class="single-tab">
												<i class="fa fa-mobile"></i>
												<h4>Pemesanan Mudah</h4>
												<p>Melakukan proses pemesanan dimanapun dengan mudah</p>
											</div>
											<!--/ End Single Tab -->
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<!-- Single Tab -->
											<div class="single-tab">
												<i class="fa fa-calendar-check-o"></i>
												<h4>Terstruktur</h4>
												<p>kami bekerja berdasarkan waktu yang sudah disepakati</p>
											</div>
											<!--/ End Single Tab -->
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<!-- Single Tab -->
											<div class="single-tab">
												<i class="fa fa-refresh"></i>
												<h4>Laporan</h4>
												<p>Memberikan laporan pengembangan project anda secara berkala</p>
											</div>
											<!--/ End Single Tab -->
										</div>
									</div>
								</div>
								<!-- <div role="tabpanel" class="tab-pane fade in" id="pesanweb">
									<div class="row">
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="single-tab">
												<i class="fa fa-lock"></i>
												<h4>Login</h4>
												<p>Masuk Kedalam Aplikasi terlebih dahulu <a href="<?php echo site_url('login') ?>" class="btn btn-success">Login</a></p>
											</div>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="single-tab">
												<i class="fa fa-shopping-cart"></i>
												<h4>Pilih Menu Pesan</h4>
												<p>Masuk Kemenu pemesanan yang tersedia, dan pilih kategori pemesanan yang ada</p>
											</div>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="single-tab">
												<i class="fa fa-clock-o"></i>
												<h4>Pesanan Dikerjakan</h4>
												<p>Pesanan yang masuk akan kami kerjakan</p>
											</div>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="single-tab">
												<i class="fa fa-file-pdf-o"></i>
												<h4>Laporan Pengerjaan</h4>
												<p>Laporan proses pengerjaan akan diberikan via web sesuai yang telah disepakati</p>
											</div>
										</div>
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="single-tab">
												<i class="fa fa-check"></i>
												<h4>Pekerjaan Selesai</h4>
												<p>Proyek yang diberikan selesai dan akan diberikan ke pemesan</p>
											</div>
										</div>
									</div>
								</div> -->
								<div role="tabpanel" class="tab-pane fade in" id="pesanchat">
									<div class="row">
										<div class="col-md-6 col-sm-6 col-xs-12">
											<div class="single-tab">
											<i class="fa fa-whatsapp"></i>
											<h4>Via Whatsapp</h4>
											<p>Untuk pemesanan via chat bisa melalui Whatsapp <a href="https://api.whatsapp.com/send?phone=6281805467977&text=Halo%20Admin%20Saya%20Mau%20Order" class="btn btn-success" target="_blank">Whatsapp</a></p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--/ End Tab Content -->
						</div>
						<!--/ End About Tab -->
					</div>
				</div>
			</div>
		</section>
		<!--/ End About Us -->
		
		<!-- Our Skill -->
		<section id="our-skill" class="section">
			<div class="container">
				<div class="row"> 
					<div class="col-md-6 col-sm-12 col-xs-12 wow fadeIn">
						<!-- Info Main -->
						<div class="info-main">
							<div class="section-title left">
								<h2>Technology<span>Info</span></h2>
							</div>
							<div class="some-info">
								<p>Berikut merupakan teknologi yang kami gunakan untuk pembuatanw website</p>
							</div>
							<ul class="info-list">
								<li><i class="fa fa-check"></i>Bootstrap</li>
								<li><i class="fa fa-check"></i>Jquery</li>
								<li><i class="fa fa-check"></i>Codeigniter</li>
								<li><i class="fa fa-check"></i>Plugin Pendukung</li>
							</ul>	
						</div>
						<!--/ End Info Main -->
					</div>				
				</div>
			</div>
		</section>
		<!--/ End Our Skill -->
		
		<!-- Start Team -->
		<section id="team" class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 wow fadeIn">
						<div class="section-title center">
							<h2><span>Team</span></h2>
						</div>
					</div>
				</div>
				<div class="row">	
					<div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.4s">
						<!-- Single Team -->
						<div class="single-team">
							<div class="team-head">
								<img src="<?php echo base_url('assets/images/team/reza.jpg') ?>" alt=""/>
							</div>
							<div class="team-info">
								<div class="name">
									<h4>Reza Akbar<span>Back-End Developer</span></h4>
								</div>
								<ul class="team-social">
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="#"><i class="fa fa-instagram"></i></a></li>
								</ul>
							</div>
						</div>
						<!--/ End Single Team -->
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.6s">	
						<!-- Single Team -->
						<div class="single-team">
							<div class="team-head">
								<img src="<?php echo base_url('assets/images/team/yokego.jpg') ?>" alt=""/>
							</div>
							<div class="team-info">
								<div class="name">
									<h4>I Wayan Yokego<span>Web Designer</span></h4>
								</div>
								<ul class="team-social">
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-instagram"></i></a></li>
								</ul>
							</div>
						</div>
						<!--/ End Single Team -->
					</div>
					<div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.8s">
						<!-- Single Team -->
						<div class="single-team active">
							<div class="team-head">
								<img src="<?php echo base_url('assets/images/team/nanang.jpg') ?>" alt=""/>
							</div>
							<div class="team-info">
								<div class="name">
									<h4>Nanang Adistia<span>Web Development</span></h4>
								</div>
								<ul class="team-social">
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-instagram"></i></a></li>
								</ul>
							</div>
						</div>
						<!--/ End Single Team -->
					</div>
				</div>
            </div>
		</section>	 
		<!--/ End Team -->		
		
		<!-- Start Testimonials -->
		<section id="testimonial" class="section wow fadeIn">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 wow fadeIn">
						<div class="section-title center">
							<h2>Our <span>Testimonials</span></h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="testimonial-carousel">	
							<!-- Single Testimonial -->
							<?php foreach($testimonial as $t){ ?>
							<div class="single-testimonial">
								<div class="testimonial-content">
									<p><?php echo $t->pesan_testimoni ?></p>
								</div>
								<div class="testimonial-info">
									<h6><?php echo $t->nama_user ?></h6>
								</div>			
							</div>
							<?php } ?>
							<!--/ End Single Testimonial -->
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/ End Testimonial -->

		<!-- Start Portfolio -->
		<section id="portfolio" class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 wow fadeIn">
						<div class="section-title center">
							<h2>Our <span>Portfolio</span></h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="portfolio-carousel">
							<!-- Single Portfolio -->
							<?php foreach($portfolio as $po){ ?>
							<div class="portfolio-single">
								<a href="<?php echo base_url('assets/images/portfolio/'.$po->path_portfolio) ?>" class="zoom">
									<div class="portfolio-head">
										<img src="<?php echo base_url('assets/images/portfolio/'.$po->path_portfolio) ?>" alt=""/>
										<i class="fa fa-search"></i>
									</div>
								</a>
								<div class="text">
									<h4><a href="<?php echo $po->url_portfolio ?>"><?php echo $po->nama_portfolio ?></a></h4>
								</div>
							</div>
							<?php } ?>
							<!--/ End Portfolio -->
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/ End Portfolio -->
		
		
		
		<!-- Start blog -->
		<section id="blog" class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12  wow fadeIn">
						<div class="section-title center">
							<h2><span>Produk</span></h2>
							<p>Berikut Aplikasi yang kami jual</p>
							<p>
								Ingin berjualan aplikasi yang anda buat ? <a href="<?php echo site_url('login') ?>" class="btn btn-success btn-flat">Jual Aplikasi</a>
							</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="blog">
							<?php foreach($produk as $p){ ?>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<!-- Single blog -->
								<div class="single-blog">
									<div class="blog-head">
										<img src="<?php echo base_url('assets/images/produk/'.$p->path_produk) ?>" alt="#">
										<a href="blog-single.html" class="link"><i class="fa fa-link"></i></a>
									</div>
									<div class="blog-content">
										<h2><a href="blog-single.html"><?php echo $p->nama_produk ?></a></h2>
										<div class="meta">
											<span><i class="fa fa-user"></i><?php echo $p->username ?></span>
											<span><i class="fa fa-money"></i>Rp <?php echo number_format($p->harga_produk,0,',','.') ?></span>
										</div>
										<p><?php echo $p->detail_produk ?></p>
										<a href="blog-single.html" class="btn">Read More<i class="fa fa-angle-double-right"></i></a>
									</div>
								</div>	
								<!--/ End Single blog -->
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!--/ End blog -->

		<!-- Contact Us -->
		<section id="contact" class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 wow fadeIn">
						<div class="section-title center">
							<h2>Contact <span>US</span></h2>
						</div>
					</div>
				</div>
				<!-- Google Map -->
				<div class="row">
					<!-- Contact Form -->
					<div class="col-md-5 col-sm-5 col-xs-12">
						<form class="form" method="post" action="<?php echo site_url('home/contact') ?>">
							<div class="form-group">
								<input type="email" name="email" placeholder="Email" required="required">
                            </div>
							<div class="form-group">
								<input type="text" name="subject" placeholder="Subject" required="required">
                            </div>
							<div class="form-group">
								<textarea name="message" rows="6" placeholder="Message" ></textarea>
                            </div>
							<div class="form-group">	
								<button type="submit" class="button primary"><i class="fa fa-send"></i>Submit</button>
                            </div>
						</form>
					</div>
					<!--/ End Contact Form -->
				</div>
			</div>
		</section>
		<!--/ End Clients Us -->
		
		<!-- Location -->
		<section id="location" class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 wow fadeIn">
						<div class="section-title center">
							<h2>Our <span>Contact</span></h2>
							<p>Untuk lebih mempermudah dan mempercepat proses respon kami, berikut informasi contact tambahan kami</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-12 wow fadeIn" data-wow-delay="0.4s">
						<!-- Single Address -->
						<div class="single-address">
							<i class="fa fa-whatsapp"></i>
							<h4>Whatsapp</h4>
							<p>081805467977</p>
						</div>
						<!--/ End Single Address -->
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 wow fadeIn" data-wow-delay="0.6s">
						<!-- Single Address -->
						<!-- <div class="single-address active">
							<i class="fa fa-phone"></i>
							<h4>Office Location</h4>
							<p>240, Khilgaon Dhaka 1230.</p>
						</div> -->
						<!--/ End Single Address -->
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 wow fadeIn" data-wow-delay="0.8s">
						<!-- Single Address -->
						<div class="single-address">
							<i class="fa fa-envelope"></i>
							<h4>E-mail</h4>
							<p>support@coffecode.com</p>
						</div>
						<!--/ End Single Address -->
					</div>

				</div>
			</div>
		</section>
		<!--/ End Location -->
		
		<!-- Start Clients -->
		<div id="clients" class="section wow fadeIn">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="clients-carousel">
							<!-- Single Clients -->
							<?php foreach($klien as $k){ ?>
							<div class="single-client">
								<a href="<?php echo $k->url_klien ?>" title="<?php echo $k->nama_klien ?>"><img src="<?php echo base_url('assets/images/klien/'.$k->path_klien) ?>" width="200px" height="50px" alt=""></a>
							</div>
							<?php } ?>
							<!--/ Single Clients -->
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/ End Clients -->		