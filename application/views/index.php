<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Coffe Code</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicons
    ================================================== -->
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="img/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

<!-- Bootstrap -->
<link rel="stylesheet" type="text/css"  href="<?php echo base_url() ?>assets/home/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/home/fonts/font-awesome/css/font-awesome.css">

<!-- Stylesheet
    ================================================== -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/home/css/style.css">
<link rel="stylesheet" type="text/css" href="<?phP echo base_url() ?>assets/home/css/nivo-lightbox/nivo-lightbox.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/home/css/nivo-lightbox/default.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Navigation
    ==========================================-->
<nav id="menu" class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand page-scroll" href="#page-top">Coffe Code</a>
      <div class="phone">
        <a href="<?php echo site_url('login') ?>" class="btn btn-custom">Login</a>
        <a href="<?php echo site_url('register') ?>" class="btn btn-custom">Register</a>
      </div>
    </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#about" class="page-scroll">About</a></li>
        <li><a href="#portfolio" class="page-scroll">Projects</a></li>
        <li><a href="#testimonials" class="page-scroll">Testimonials</a></li>
        <li><a href="#contact" class="page-scroll">Contact</a></li>
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
</nav>
<!-- Header -->
<header id="header">
  <div class="intro">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2 intro-text">
            <div id="example">
              <h1>Welcome</h1>
              <h1>Coffe Code</h1>
            </div>
            <!-- <h1>Coffe Code</h1> -->
            <p>Jasa Pembuatan Website Sistem Informasi dan Aplikasi Berbasis Web</p>
            <a href="#about" class="btn btn-custom btn-lg page-scroll">Learn More</a> </div>
        </div>
      </div>
    </div>
  </div>
</header>
<!-- Get Touch Section -->
<div id="get-touch">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-6 col-md-offset-1">
        <h3>Buat Website Anda Sekarang</h3>
        <p>Mulai kembangkan usaha anda dan hobi anda menjelajahi internet</p>
      </div>
      <div class="col-xs-12 col-md-4 text-center"><a href="#contact" class="btn btn-custom btn-lg page-scroll">Free Estimate</a></div>
    </div>
  </div>
</div>
<!-- About Section -->
<div id="about">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-6"> <img src="<?php echo base_url() ?>assets/home/img/about.jpg" class="img-responsive" alt=""> </div>
      <div class="col-xs-12 col-md-6">
        <div class="about-text">
          <h2>Who We Are</h2>
          <p>Coffe Code merupakan media jasa pembuatan website untuk keperluan personal maupun kelompok, seperti website blog pribadi, toko online, company profile, dan sistem informasi. Coffe Code siap melayani pembuatan website anda hingga siap diluncurkan</p>
          <h3>Why Choose Us?</h3>
          <div class="list-style">
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <ul>
                <li>Biaya Terjangkau</li>
                <li>Responsive Web</li>
                <li>Dokumentasi Lengkap</li>
                <li>Proses pemesanan mudah</li>
              </ul>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
              <ul>
                
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Gallery Section -->
<div id="portfolio">
  <div class="container">
    <div class="section-title">
      <h2>Produk Sales</h2>
    </div>
    <div class="row">
      <?php foreach($produk as $p){ ?>
      <div class="portfolio-items">
        <div class="col-sm-6 col-md-4 col-lg-4">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="<?php echo base_url('assets/images/produk/'.$p->path_produk) ?>" title="Project Title" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4><?php echo $p->nama_produk ?></h4>
              </div>
              <img src="<?php echo base_url('assets/images/produk/'.$p->path_produk) ?>" class="img-responsive" alt="Project Title"> </a> </div>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
</div>
<!-- Testimonials Section -->
<div id="testimonials">
  <div class="container">
    <div class="section-title">
      <h2>Testimonials</h2>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="testimonial">
          <div class="testimonial-image"> <img src="<?php echo base_url() ?>assets/home/img/testimonials/01.jpg" alt=""> </div>
          <div class="testimonial-content">
            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed dapibus leo nec ornare diam sedasd commodo nibh ante facilisis bibendum dolor feugiat at."</p>
            <div class="testimonial-meta"> - John Doe </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Contact Section -->
<div id="contact">
  <div class="container">
    <div class="col-md-8">
      <div class="row">
        <div class="section-title">
          <h2>Get In Touch</h2>
          <p>Informasi Lebih Lanjut Perihal Pemesanan &amp; Konsultasi</p>
        </div>
        <form action="" method="post">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <input type="text" id="name" class="form-control" name="nama" placeholder="Nama Lengkap" required="required">
                <p class="help-block text-danger"></p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <input type="email" id="email" class="form-control" name="email" placeholder="Email" required="required">
                <p class="help-block text-danger"></p>
              </div>
            </div>
          </div>
          <div class="form-group">
            <textarea name="message" id="message" class="form-control" rows="4" placeholder="Pesan atau Pertanyaan" required></textarea>
            <p class="help-block text-danger"></p>
          </div>
          <div id="success"></div>
          <button type="submit" class="btn btn-custom btn-lg"><i class="fa fa-send"></i> Send Message</button>
        </form>
      </div>
    </div>
    <div class="col-md-3 col-md-offset-1 contact-info">
      <div class="contact-item">
        <h4>Contact Info</h4>
        <p><span><i class="fa fa-map"></i>&nbsp;Address</span>Jalan Pakusari No 10A<br>
          Sesetan, Denpasar, Bali</p>
      </div>
      <div class="contact-item">
        <p><span><i class="fa fa-whatsapp"></i>&nbsp;Phone</span> +6281805467977</p>
      </div>
      <div class="contact-item">
        <p><span><i class="fa fa-envelope"></i>&nbsp;Email</span> support@coffecode.com</p>
      </div>
    </div>
    <div class="col-md-12">
      <div class="row">
        <div class="social">
          <ul>
            <li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Footer Section -->
<div id="footer">
  <div class="container text-center">
    <p>&copy; 2018 CoffeCode.</p>
  </div>
</div>
<script type="text/javascript" src="<?php echo base_url() ?>assets/home/js/jquery.1.11.1.js"></script> 
<script type="text/javascript" src="<?php echo base_url() ?>assets/home/js/bootstrap.js"></script> 
<script type="text/javascript" src="<?php echo base_url() ?>assets/home/js/SmoothScroll.js"></script> 
<script type="text/javascript" src="<?php echo base_url() ?>assets/home/js/nivo-lightbox.js"></script> 
<script type="text/javascript" src="<?php echo base_url() ?>assets/home/js/jqBootstrapValidation.js"></script> 
<script type="text/javascript" src="<?php echo base_url() ?>assets/home/js/contact_me.js"></script> 
<script type="text/javascript" src="<?php echo base_url() ?>assets/home/js/main.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/plugin/textition/textition.js"></script>
<script type="text/javascript">
 $(document).ready(function() {
   $('#example').textition({
      handler: 'mouseenter',
      animation: 'ease-in-out',
      speed: 1
   });
});
</script>
</body>
</html>