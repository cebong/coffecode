<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_produk extends CI_Model {
	
		function list_produk(){
			$this->db->select('produk.*,user.*');
			$this->db->join('user','user.id_user=produk.id_user');
			return $this->db->get('produk');
		}

		function list_produk_user(){
			$this->db->select('produk.*,user.*');
			$this->db->join('user','user.id_user=produk.id_user');
			$this->db->order_by('id_produk','DESC');
			$this->db->like('id_user',$this->session->userdata('id'));
			return $this->db->get('produk');
		}

		function create($data,$table){
			$this->db->insert($table,$data);
		}
	
	}
	
	/* End of file M_produk.php */
	/* Location: ./application/models/M_produk.php */
?>