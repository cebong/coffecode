<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_portfolio extends CI_Model {
	
		function list_portfolio(){
			return $this->db->get('portfolio');
		}
	
	}
	
	/* End of file M_portfolio.php */
	/* Location: ./application/models/M_portfolio.php */
?>