<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_testimoni extends CI_Model {
	
		function list_testimoni(){
			$this->db->select('user.*,testimoni.*');
			$this->db->join('user','user.id_user=testimoni.id_user');
			$this->db->order_by('id_testimoni','ASC');
			return $this->db->get('testimoni');
		}
	
	}
	
	/* End of file M_testimoni.php */
	/* Location: ./application/models/M_testimoni.php */
?>