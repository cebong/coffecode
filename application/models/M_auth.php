<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_auth extends CI_Model {
	
		function cek_login($username,$table){
			$this->db->where('username',$username);
			return $this->db->get($table);
		}

		function daftar($data,$table){
			$this->db->insert($table,$data);
		}
	
	}
	
	/* End of file M_auth.php */
	/* Location: ./application/models/M_auth.php */
?>